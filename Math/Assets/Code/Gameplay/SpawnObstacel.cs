﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacel : MonoBehaviour
{
    public GameObject Obstacle;
    private int Pattern;
    private float TimetoSpawn;
    void Start()
    {
        Randomlane();
    }

    void Update()
    {
        TimetoSpawn += Time.deltaTime;
        if (TimetoSpawn >= Score.instant.TimeToSpawn)
        {
            Randomlane();
            TimetoSpawn = 0;
        }
    }

    private void Randomlane()
    {
        Pattern = Random.Range(1, 4);
        switch (Pattern)
        {
            case 1:Instantiate(Obstacle, new Vector3(-3, -0.7f, 265), Quaternion.identity);
                break;
            case 2:Instantiate(Obstacle, new Vector3(0, -0.7f, 265), Quaternion.identity);
                break;
            case 3:Instantiate(Obstacle, new Vector3(3, -0.7f, 265), Quaternion.identity);
                break;
        }
    }
}
