﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation : MonoBehaviour
{
    private Animator _animator;
    void Start()
    {
        _animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") )
        {
            _animator.SetBool("Jump",true);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ground")
        {
            _animator.SetBool("Jump", false);
        }
    }
}
