﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    [SerializeField] AudioClip m_Jump;
    [SerializeField] AudioClip m_Swap;
    [SerializeField] AudioClip m_Hit;
    [SerializeField] AudioClip m_Goal;
    [SerializeField] AudioClip m_Ping;
    [SerializeField] AudioClip m_MouseOver;
    [SerializeField] AudioSource m_source;
    public static SFX instance;
    void Start()
    {
        instance = this;
        m_source = GetComponent<AudioSource>();
    }

    public void PlayJump()
    {
        m_source.PlayOneShot(m_Jump,m_source.volume);
    }
    public void PlaySwap()
    {
        m_source.PlayOneShot(m_Swap,m_source.volume);
    }
    public void PlayHit()
    {
        m_source.PlayOneShot(m_Hit,m_source.volume);
    }
    public void PlayGoal()
    {
        m_source.PlayOneShot(m_Goal,m_source.volume);
    } 
    public void PlayPing()
    {
        m_source.PlayOneShot(m_Ping,m_source.volume);
    }
    public void PlayMouseOVer()
    {
        m_source.PlayOneShot(m_MouseOver,m_source.volume);
    }
}
