﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Playercontrol : MonoBehaviour
{
    private int max = 2;
    public Rigidbody rb;
    private float CoolDownJump = 2;
    public bool Hit;
    public static Playercontrol instant;
    public bool ToPlaySoundGoal;
    public float TimeToGoal;
    public AudioSource Source;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        instant = this;
    }

    void Update()
    {
        if(CoolDownJump <=5)
            CoolDownJump += Time.deltaTime;
        if (Playercontrol.instant.Hit == false)
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow) && max != 1)
            {
                transform.position = transform.position + new Vector3(-3f, 0, 0);
                max -= 1;
                SFX.instance.PlaySwap();
            }

            if (Input.GetKeyDown(KeyCode.RightArrow) && max != 3)
            {
                transform.position = transform.position + new Vector3(3f, 0, 0);
                max += 1;
                SFX.instance.PlaySwap();
            }

            if (Input.GetButtonDown("Jump") && CoolDownJump >= 1.7)
            {
                rb.AddForce(new Vector3(0, 8, 0), ForceMode.Impulse);
                CoolDownJump = 0;
                SFX.instance.PlayJump();
            }

            if (TimeToGoal <= 6)
            {
                TimeToGoal += Time.deltaTime;
                ToPlaySoundGoal = true;
            }

            
            
        }
    }

    void OnTriggerEnter(Collider target)
        {
            if (target.tag == "Obstacle")
            {
                Hit = true;
                SFX.instance.PlayHit();
            }
            if (target.tag == "Choice")
            {
                if (TimeToGoal >= 5 && ToPlaySoundGoal == true)
                {
                    ToPlaySoundGoal = false;
                    TimeToGoal = 0;
                    SFX.instance.PlayGoal();
                    
                }
            }
        }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Choice")
        {
            Mathshowtext.instant.SwitchShowMath = false;
        }
    }
}
