﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public GameObject PausePanel;
    public TMP_Text Scoretext;
    public float ScoreRun;
    public static Score instant;
    public float Speed=0.1f;
    public float TimeToSpawn=0.1f;
    void Start()
    {
        Scoretext = GetComponent<TMP_Text>();
        instant = this;
    }

    private void Update()
    {
        if (Playercontrol.instant.Hit == false)
        {
            if (Input.GetKeyDown(KeyCode.S) && Time.timeScale == 0)
            {
                Time.timeScale = 1;
                PausePanel.SetActive(false);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                Time.timeScale = 0;
                PausePanel.SetActive(true);
            }
        }

    }

    void FixedUpdate()
    {
        if (Playercontrol.instant.Hit == false)
        {
            ScoreRun += Time.deltaTime * 100;
            Scoretext.text = (Math.Round(ScoreRun)).ToString();
        }

        
        /*if(ScoreRun >= 2000)
        {
            Speed = -1.2f;
            TimeToSpawn = 0.8f;
        }
        if(ScoreRun >= 3000)
        {
            Speed = -1.5f;
            TimeToSpawn = 0.5f;
        }
        if(ScoreRun >= 4000)
        {
            Speed = -1.8f;
            TimeToSpawn = 0.3f;
        }*/
    }
}
