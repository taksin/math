﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loss : MonoBehaviour
{
    public GameObject Losspanel;
    
    // Update is called once per frame
    void Update()
    {
        if (Playercontrol.instant.Hit == true )
        {
            Whenloss();
        }
    }

    void Whenloss()
    {
        Losspanel.SetActive(true);
    }
}
