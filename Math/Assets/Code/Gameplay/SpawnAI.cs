﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAI : MonoBehaviour
{
    public GameObject truecube;
    public GameObject wrongcube1;
    public GameObject wrongcube2;
    public int Randomlane;
    public bool respawn=true;
    public static SpawnAI instant;
    public float time;
    void Start()
    {
        instant = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (time <= 8)
            time += Time.deltaTime;
        
        if (respawn == true && time >= 8)
        {
            Mathshowtext.instant.SwitchShowMath = true;
            time = 0;
            respawn = false;
            randomlane();
        }

    }

    void randomlane()
    {
        SFX.instance.PlayPing();
            Randomlane = Random.Range(1, 4);
            switch (Randomlane)
            {
                case 1:
                    Instantiate(truecube, new Vector3(-3, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube1, new Vector3(0, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube2, new Vector3(3, 0, 120), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(truecube, new Vector3(0, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube1, new Vector3(-3, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube2, new Vector3(3, 0, 120), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(truecube, new Vector3(3, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube1, new Vector3(0, 0, 120), Quaternion.identity);
                    Instantiate(wrongcube2, new Vector3(-3, 0, 120), Quaternion.identity);
                    break;
            }
        
    }
}
