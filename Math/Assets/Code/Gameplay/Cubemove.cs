﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Cubemove : MonoBehaviour
{
    public Mesh[] meshes;

    private void Start()
    {
        GetComponent<MeshFilter>().mesh = meshes[Random.Range(0, meshes.Length)];
    }
    void FixedUpdate()
    {
        if (Playercontrol.instant.Hit == false)
        {
            transform.position = transform.position + new Vector3(0, 0, Score.instant.Speed);
            if (transform.position.z <= -40)
            {
                Mathshowtext.instant.Randoms = Random.Range(1, 4);
                Mathshowtext.instant.Randomagain = true;
                SpawnAI.instant.respawn = true;
                Destroy(gameObject);
            }
        }
    }
}
