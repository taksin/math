﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Mathshowtext : MonoBehaviour
{
    [SerializeField]
    private Text A;
    [SerializeField]
    private Text B;
    [SerializeField]
    private TMP_Text Plus;

    public bool Randomagain;
    private int math1;
    private int math2;
    public int math3;
    public int Randoms;
    public static Mathshowtext instant;
    [SerializeField]
    private GameObject ShowMath;
    public bool SwitchShowMath;
    void Start()
    {
        Randommath();
        instant = this;
    }

    void Update()
    {
        if (Score.instant.ScoreRun <= 3000)
        {
            Plus.text = "+";
            math3 = math1 + math2;
        }
        if (Score.instant.ScoreRun >= 3000)
        {
            if (Randoms == 1)
            {
                Plus.text = "+";
                math3 = math1 + math2;
            }
            if (Randoms == 2)
            {
                Plus.text = "-";
                math3 = math1 - math2;
            }
            if (Randoms == 3)
            {
                Plus.text = "-";
                math3 = math1 - math2;
            }
        } 
        if (Score.instant.ScoreRun >= 6000)
        {
            if (Randoms == 1)
            {
                Plus.text = "+";
                math3 = math1 + math2;
            }
            if (Randoms == 2)
            {
                Plus.text = "-";
                math3 = math1 - math2;
            }
            if (Randoms == 3)
            {
                Plus.text = "x";
                math3 = math1 * math2;
            }
        }
        if (SwitchShowMath == true)
        {
            ShowMath.SetActive(true);
        }
        if (SwitchShowMath == false)
        {
            ShowMath.SetActive(false);
        }
        A.text = math1.ToString();
        B.text = math2.ToString();
        //C.text = math3.ToString();
        if (Score.instant.ScoreRun <= 3000)
        {
            math3 = math1 + math2;
        }
       
            
        

        if (Randomagain == true)
        {
            Randomagain = false;
            Randommath();
            Debug.Log("Random");
        }
    }

    public void Randommath()
    {
        math1 = Random.Range(1, 10);
        math2 = Random.Range(1, 10);
    }
}
