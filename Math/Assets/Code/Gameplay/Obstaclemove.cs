﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaclemove : MonoBehaviour
{
    private float Near;
    void Start()
    {

    }

    void FixedUpdate()
    {
        if (Playercontrol.instant.Hit == false)
        {
            transform.position = transform.position + new Vector3(0, 0, Score.instant.Speed);
            if (transform.position.z <= -40)
            {
                Destroy(gameObject);
            }
        }

        Near =  GameObject.FindWithTag("Choice").transform.position.z - this.gameObject.transform.position.z ;
        if (Near <= 6 && Near >= -6)
        {
            Destroy(gameObject);
            
        }
    }
    void OnTriggerStay(Collider target)
    {
        if (target.tag == "Choice")
        {
            Destroy(gameObject);
        }
        if (target.tag == "Obstacle")
        {
            Destroy(gameObject);
        }
            
    }
}
