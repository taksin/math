﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StartGame : MonoBehaviour
{
    [SerializeField] private GameObject CreditShow;
    public void StartToGamePlay(int SampleScene)
    {
        SceneManager.LoadScene(SampleScene);
        SFXMENU.instance.PlayClick();
    }

    public void OnMouseOver()
    {
        SFXMENU.instance.PlayOver();
    }

    public void ShowCredit()
    {
        CreditShow.SetActive(true);
        SFXMENU.instance.PlayClick();
    }
    public void ExitShowCredit()
    {
        CreditShow.SetActive(false);
        SFXMENU.instance.PlayClick();
    }

    public void ExitGame()
    {
        Application.Quit();
        SFXMENU.instance.PlayClick();
        Debug.Log("Over");
    }
}
