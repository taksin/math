﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXMENU : MonoBehaviour
{
    [SerializeField] AudioClip m_Over;
    [SerializeField] AudioClip m_Click;
    [SerializeField] AudioSource m_source;
    public static SFXMENU instance;
    void Start()
    {
        m_source = GetComponent<AudioSource>();
        instance = this;
    }
    public void PlayOver()
    {
        m_source.PlayOneShot(m_Over,m_source.volume);
    }
    public void PlayClick()
    {
        m_source.PlayOneShot(m_Click,m_source.volume);
    }
}
