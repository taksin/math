﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSphere : MonoBehaviour
{
    [SerializeField] private GameObject Obstacle;
    [SerializeField] private GameObject Obstacle1;
    [SerializeField] private GameObject Obstacle2;
    [SerializeField] private GameObject Obstacle3;
    [SerializeField] private GameObject Obstacle4;
    private float TimeToSpawn;
    private int RandomSpehere;
        
    // Update is called once per frame
    void Update()
    {
        TimeToSpawn += Time.deltaTime;
        if (TimeToSpawn >=1)
        {
            RandomSpehere = Random.Range(1, 6);
            switch (RandomSpehere)
            {
                case 1: Instantiate(Obstacle, new Vector3(Random.Range(-8,8), 10, 620), Quaternion.identity);
                    break;
                case 2: Instantiate(Obstacle1, new Vector3(Random.Range(-8,8), 10, 620), Quaternion.identity);
                    break;
                case 3: Instantiate(Obstacle2, new Vector3(Random.Range(-8,8), 10, 620), Quaternion.identity);
                    break;
                case 4: Instantiate(Obstacle3, new Vector3(Random.Range(-8,8), 10, 620), Quaternion.identity);
                    break;
                case 5: Instantiate(Obstacle4, new Vector3(Random.Range(-8,8), 10, 620), Quaternion.identity);
                    break;
            }
            
            TimeToSpawn = 0;
        }
    }
}
