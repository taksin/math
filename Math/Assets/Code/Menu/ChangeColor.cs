﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    private int ColorChangeMat;
    [SerializeField] private Renderer Mymat;
    void Start()
    {
        ColorChangeMat = Random.Range(1, 5);
        Mymat.material.color = Color.blue;
    }

    void Update()
    {
        switch (ColorChangeMat)
        {
            case 1: Mymat.material.color = Color.blue;
                break;
            case 2: Mymat.material.color = Color.red;
                break;
            case 3: Mymat.material.color = Color.green;
                break;
            case 4: Mymat.material.color = Color.yellow;
                break;
        }
    }
}
